from nltk import RegexpParser
from nltk.tree import Tree
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize
import nltk
nltk.download('averaged_perceptron_tagger')

def is_imperative(tagged_sent):
    if len(tagged_sent)<6: # very short sentences
        return "statefact"
    elif tagged_sent[0][0]=='yes':
        return "statefact"
    else:
    # if the sentence is not a question...
        if tagged_sent[-1][0] != "?":
            # starts with verbs"
            if tagged_sent[0][1] == "VB" or tagged_sent[0][1] == "MD": # sentences of the form : "do this OR can this be done"
                return "action_to_be_taken"
            else:
                chunk = get_chunks(tagged_sent)
                # check if the first chunk of the sentence is a VB-Phrase
                # check if root of sentence is verb
                if isinstance(chunk[0], Tree):
                    if chunk[0].label() == "VB-Phrase":
                        ## check whether it is stating stuff or providing feedback to do something
                        for tt in range(len(tagged_sent)-1):
                            print("tt",tt)
                            if tagged_sent[tt][1] == "NN" and (tagged_sent[tt+1][1] == "VBP" or tagged_sent[tt+1][1] == "VBZ") and (tagged_sent[tt+1][0] == 'is' or tagged_sent[tt+1][0] == 'are' or tagged_sent[tt+1][0] == 'am'):  # we are; i am
                                # print("tagged_sent", tagged_sent)
                                return "statefact"

                        if tagged_sent[0][1] == "PRP" and tagged_sent[1][1] == "VBP" and (tagged_sent[1][0] == 'is' or tagged_sent[1][0] == 'are' or tagged_sent[1][0] == 'am'):  # we are; i am
                            # print("tagged_sent", tagged_sent)
                            return "statefact"
                        elif tagged_sent[0][1] == "DT" and (tagged_sent[1][1]== "VBZ" or tagged_sent[1][1]== "VBP") and (tagged_sent[1][0]=='is' or tagged_sent[1][0]=='are'): # this is:
                            return "statefact"
                        elif tagged_sent[0][1] == "EX" and tagged_sent[1][1]== "VBZ" and (tagged_sent[1][0]=='is' or tagged_sent[1][0]=='are'): # there is a
                            # print("tagged_sent", tagged_sent)
                            return "statefact"
                        elif tagged_sent[0][1] == "NNS" and (tagged_sent[1][1]== "VBZ" or tagged_sent[1][1]== "VBP") and (tagged_sent[1][0]=='is' or tagged_sent[1][0]=='are'): # e.g. customers are ...
                            # print("tagged_sent", tagged_sent)
                            return "statefact"
                        elif tagged_sent[0][1] == "DT" and tagged_sent[1][1] == "NNS" and (tagged_sent[2][1]== "VBZ" or tagged_sent[2][1]== "VBP") and (tagged_sent[2][0]=='is' or tagged_sent[2][0]=='are'): # e.g. customers are ...
                            # print("tagged_sent", tagged_sent)
                            return "statefact"
                        else:
                            return "action_to_be_taken"
                    else:
                        return "statefact"
                else:
                        ## if a verb phrase is not the root of the sentence
                        if tagged_sent[0][1] == "NNP" and tagged_sent[1][1] == "VBP" and tagged_sent[1][0]!='is' and tagged_sent[1][0]!='are':  # ERICSSON is a
                            # print("tagged_sent", tagged_sent)
                            return "action_to_be_taken"
                        else:
                            count=1
                            for tt in range(len(tagged_sent)-1):
                                print("tt",tt)
                                if tagged_sent[tt][1]=='MD' and tagged_sent[tt+1][1]=='VB' and (tagged_sent[tt][1]=='should'): ## can do this; should do this; could do this;
                                   print("hello")
                                   count=0
                            if count==0:
                                return "action_to_be_taken"
                            else:
                                return "statefact"




# chunks the sentence into grammatical phrases based on its POS-tags
def get_chunks(tagged_sent):
    chunkgram = r"""VB-Phrase: {<DT><,>*<VB>} # determiner like "this" followed by verb
                    VB-Phrase: {<RB><VB>} # adverb followed by verb
                    VB-Phrase: {<PRP><VB>} # personal pronoun followed by verb
                    VB-Phrase: {<NN.?>+<,>*<VB>} # noun followed by verb
                    VB-Phrase: {<EX.?>+<,>*<VB>} # existential followed by verb
                    VB-Phrase: {<DT><NNS>*<VB>} 
                    VB-Phrase: {<DT><NNS>*<VBP>} 
                    VB-Phrase: {<DT><,>*<VBP>}
                    VB-Phrase: {<RB><VBP>}
                    VB-Phrase: {<PRP><VBP>}
                    VB-Phrase: {<NN.?>+<,>*<VBP>}
                    VB-Phrase: {<EX.?>+<,>*<VBP>} # existential followed by verb
                    VB-Phrase: {<DT><,>*<VBZ>}
                    VB-Phrase: {<RB><VBZ>}
                    VB-Phrase: {<PRP><VBZ>}
                    VB-Phrase: {<NN.?>+<,>*<VBZ>}
                    VB-Phrase: {<NNS.?>+<,>*<VBZ>}
                    VB-Phrase: {<NNS.?>+<,>*<VBP>}
                    VB-Phrase: {<EX.?>+<,>*<VBZ>} # existential followed by verb
                    Q-Tag: {<,><MD><RB>*<PRP><.>*}"""
    chunkparser = RegexpParser(chunkgram)
    aa=chunkparser.parse(tagged_sent)
    print("chunkparser",aa)
    return chunkparser.parse(tagged_sent)
# aa="Ericsson is awesome!! Just ask the customers."
# sample_text="ericsson engineer needs open area to think, innovate or delivery at best, but some time behaiour of  local region response make conditons different way. Lets have a smooth handshake with region or else do not expect the best."
# sample_text="Customer first is just not the case in many areas. We need to change how e measure success. Customer KPIs need to be installed on all levels. Customer perspective should be the driver to resolve contradictions and resource conflicts between our teams/departments, currently we resolve conflicts by starvation (we do not make a decision until it is far too late to change the game to better serve the customer), product management and architecture need to make their decisions quickler and more transparent. Management need to make sure that investments are preserved."
# sample_text="Ericsson in a way still have industrial age mindset to push thing out to our customers. ItÃ¢Â€Â™s very hard to get customization on the fly for almost all itÃ¢Â€Â™s portfolio. We need to build everything with our customers in the center. And weÃ¢Â€Â™re not there yet."
# sample_text="Serviceability and simplicity in our offerings need to be improved. We need to start think more on delivering solutions rather than bits and pieces to enhance the customer experience."
# sample_text="After the decline in technology, I see that change of mentality that needs to reach the new  technological leadership."
# sample_text="As closely working with Customer, I can see many gaps on customer support and Solution delivery
# aa=sample_text
# aa="It takes less than half the time for the competition to solve a problem."
# aa="We need to improve."
# aa="I'm not sure."
# tokens = word_tokenize(aa.lower())
# pos_tags = pos_tag(tokens)
# print(pos_tags)
# print(len(pos_tags))
# print(pos_tags[0][0])
# print(pos_tags[0][1])
# result1=is_imperative(pos_tags)
# print(result1)
import pandas as pd
survey_data=pd.read_csv('cust_focus.csv')
from nltk.tokenize import sent_tokenize
comments=survey_data['Answer']
imperative_sentences=[]
non_imperative_sentences=[]
number_of_imperative_sentences=0
comments_withfeedback=[]
for tt in comments:
    aa=sent_tokenize(tt) # for each comment
    for kk in aa: # for each sentence in this comment
            count=0
            # print(kk)
            tokens = word_tokenize(kk.lower())
            pos_tags = pos_tag(tokens)
            # print(pos_tags)
            result1=is_imperative(pos_tags)
            # print(result1)
            if result1=="action_to_be_taken":
                print(kk)
                count=count+1
                imperative_sentences.append(kk)
            else:
                non_imperative_sentences.append(kk)
    if count!=0:
          number_of_imperative_sentences=number_of_imperative_sentences+1
          comments_withfeedback.append(tt)
print(number_of_imperative_sentences)
pd.DataFrame(imperative_sentences).to_csv("imperative_sent_5.csv")
pd.DataFrame(non_imperative_sentences).to_csv("nonimperative_sent_5.csv")
pd.DataFrame(comments_withfeedback).to_csv("useful_comments_5.csv")

## filter out comments with action verbs : "need","should","has to be","must be",
# else:
#     ## if its a question
#     chunk = get_chunks(tagged_sent)
#     if isinstance(chunk[-1], Tree) and chunk[-1].label() == "Q-Tag":
#         if (chunk[0][1] == "VB" or (isinstance(chunk[0],Tree) and chunk[0].label() == "VB-Phrase")):
#             return True
#         else:
#
#             return False
#     else:
#
#         return False