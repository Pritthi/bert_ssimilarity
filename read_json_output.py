import json
json_file_path ='output.jsonl'
with open(json_file_path, 'r') as j:
    contents = json.loads(j.read())
embs=[]
for tt in range(len(contents['features']))    :
    if tt!=0: ## not taking the embedding of the CLS token
        token_emb_details=contents[tt]
        if token_emb_details['token']!='SEP':
            emb_values=token_emb_details['layers'] ## length of emb_values = no of layers whose activations we are taking
            for gg in len(emb_values):
                xx=emb_values[gg]
                embs.append(xx['values'])