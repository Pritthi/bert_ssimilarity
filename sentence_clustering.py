!pip3 install flair
import flair
from google.colab import files
files.upload()
import pandas as pd

# data = pd.read_csv("all_surveys_train_sets.csv").drop_duplicates()
data = pd.read_csv("cust_focus_all_comments_THREE_Surveys_combined__calssification_output2.csv")
import re
import string


# from nltk.stem import SnowballStemmer
def clean_text(text):
    ## Remove puncuation
    text = text.translate(string.punctuation)

    ## Convert words to lower case and split them
    text = text.lower().split()

    # ## Remove stop words
    # stops = set(stopwords.words("english"))
    # text = [w for w in text if not w in stops and len(w) >= 3]

    text = " ".join(text)
    ## Clean the text
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
    return text
# apply the above function to df['text']
data['Answer'] = data['Answer'].astype('str').map(lambda x: clean_text(x))
#########################################
import numpy as np
from sklearn.model_selection import StratifiedShuffleSplit
data = data[['sentiment', 'Answer']].rename(columns={"Answer":"text", "sentiment":"label"})
data['label'] = '__label__' + data['label'].astype(str)
X=np.array(data['text'])
y=np.array(data['label'])
#############################
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=0)
for train_index, nontrain_index in sss.split(X, y):
   print("TRAIN:", train_index, "NON TRAIN:", nontrain_index)
   X_train, X_nontrain = X[train_index], X[nontrain_index]
   y_train, y_nontrain = y[train_index], y[nontrain_index]
##################################
sss = StratifiedShuffleSplit(n_splits=1, test_size=0.5, random_state=0)
for vali_index, test_index in sss.split(X_nontrain, y_nontrain):
   print("VALI:", vali_index, "TEST:", test_index)
   X_vali, X_test = X_nontrain[vali_index], X_nontrain[test_index]
   y_vali, y_test = y_nontrain[vali_index], y_nontrain[test_index]

data_train=pd.DataFrame({'label':y_train,'text':X_train})
data_test=pd.DataFrame({'label':y_test,'text':X_test})
data_vali=pd.DataFrame({'label':y_vali,'text':X_vali})

print("train value counts", data_train['label'].value_counts())
print("test value counts",data_test['label'].value_counts())
print("vali value counts",data_vali['label'].value_counts())

data_train.to_csv('train.csv', sep='\t', index = False, header = False)  # 80% of data for training
data_test.to_csv('test.csv', sep='\t', index = False, header = False) # 10% of data for testing
data_vali.to_csv('dev.csv', sep='\t', index = False, header = False) # 10% of data for dev
##################################
!pip3 install allennlp
#########################
from flair.data_fetcher import NLPTaskDataFetcher
from flair.embeddings import WordEmbeddings, FlairEmbeddings, DocumentLSTMEmbeddings,BertEmbeddings,ELMoEmbeddings
from flair.models import TextClassifier
from flair.trainers import ModelTrainer
from pathlib import Path
corpus = NLPTaskDataFetcher.load_classification_corpus(Path('./'), test_file='test.csv', dev_file='dev.csv', train_file='train.csv')
word_embeddings = [WordEmbeddings('glove'),BertEmbeddings('bert-base-uncased')]
document_embeddings = DocumentLSTMEmbeddings(word_embeddings, hidden_size=512, reproject_words=True, reproject_words_dimension=256)
classifier = TextClassifier(document_embeddings, label_dictionary=corpus.make_label_dictionary(), multi_label=False)
trainer = ModelTrainer(classifier, corpus)
trainer.train('./', max_epochs=100,learning_rate=0.5)
##############################
document_embeddings = classifier.document_embeddings ## doc embeddings based on sentiment classification
#################### -- for test set
from flair.models import TextClassifier
from flair.data import Sentence
classifier = TextClassifier.load('./best-model.pt')
count=0
misclassified_sentences_simplification=[]
true_labels_simplification_misclassified=[]
predicted_labels=[]
for tt in surveys_simplification['Answer']:
  # print(tt)
  sentence=Sentence(tt)
  # print(sentence)
  classifier.predict(sentence)
  label = str(sentence.labels[0]).split()[0]
  predicted_labels.append(label)
##################################### -- or just normal pooled embedding
document_embeddings = DocumentPoolEmbeddings([embeddings], fine_tune_mode='nonlinear')
#############################
document_embeddings = DocumentPoolEmbeddings([embeddings], pooling='mean')
##################  -- for getting the embeddings
X = [ sent.get_embedding().detach().numpy() for sent in dataset["sentence"]]
## Basically, the detach().numpy() part which will make it inputable into other models.
################# -- k means clustering
import matplotlib.pyplot as plt
wcss=[]
number_clusters=10
for i in range(1,number_clusters):
    kmeans=KMeans(n_clusters=i,init='k-means++',random_state=42)
    kmeans.fit(X)
    wcss.append(kmeans.inertia_)
plt.plot(range(1,number_clusters))
################## -- hierarchical clustering
Z=hierarchy.linkage(X,'ward')
dn=hierarchy.dendrogram(Z)
######################### -- get the most similar sentences to each sentence
import nmslib

NTHREADS = 8
def create_index(a):
    index = nmslib.init(space='angulardist')
    index.addDataPointBatch(a)
    index.createIndex()
    return index
def get_knns(index, vecs, k=3):
    return zip(*index.knnQueryBatch(vecs, k=k,num_threads=NTHREADS))
nn_wvs = create_index(embs)
to_frame = lambda x: pd.DataFrame(np.array(x)[:,1:])
idxs, dists = map(to_frame, get_knns(nn_wvs, embs, k=10))
catted = pd.concat([idxs.stack().to_frame('idx'), dists.stack().to_frame('dist')], axis=1).reset_index().drop('level_1',1).rename(columns={'level_0': 'v1', 'idx': 'v2'})
################## -- t-sne
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
model = TSNE(n_components=2, random_state=0)
np.set_printoptions(suppress=True)
Y = model.fit_transform(X)
########### -- alternative method for bert
pip install bert-serving-server  # server
pip install bert-serving-client  # client, independent of `bert-serving-server`
bert-serving-start -model_dir /your_model_directory/ -num_worker=4
from bert_serving.client import BertClient
bc = BertClient()
vectors=bc.encode(your_list_of_sentences)
################### -- google sentence encoder
import tensorflow as tf
import tensorflow_hub as hub
#download the model to local so it can be used again and again
!mkdir ../sentence_wise_email/module/module_useT
# Download the module, and uncompress it to the destination folder.
!curl -L "https://tfhub.dev/google/universal-sentence-encoder-large/3?tf-hub-format=compressed" | tar -zxvC ../sentence_wise_email/module/module_useT
######################
#Function so that one session can be called multiple times.
#Useful while multiple calls need to be done for embedding.
import tensorflow as tf
import tensorflow_hub as hub
def embed_useT(module):
    with tf.Graph().as_default():
        sentences = tf.placeholder(tf.string)
        embed = hub.Module(module)
        embeddings = embed(sentences)
        session = tf.train.MonitoredSession()
    return lambda x: session.run(embeddings, {sentences: x})
embed_fn = embed_useT('../sentence_wise_email/module/module_useT')
messages = [
    "we are sorry for the inconvenience",
    "we are sorry for the delay",
    "we regret for your inconvenience",
    "we don't deliver to baner region in pune",
    "we will get you the best possible rate"
]
embed_fn(messages)
######################
encoding_matrix = embed_fn(messages)
import numpy as np
np.inner(encoding_matrix, encoding_matrix)
#################################
#It takes similarity matrix (generated from sentence encoder) as input and gives index of redundant statements
def redundant_sent_idx(sim_matrix):
    dup_idx = []
    for i in range(sim_matrix.shape[0]):
        if i not in dup_idx:
            tmp = [t+i+1 for t in list(np.where( sim_matrix[i][i+1:] > 0.8 )[0])]
            dup_idx.extend(tmp)
    return dup_idx
#indexes of duplicate statements.
dup_indexes  = redundant_sent_idx(np.inner(encoding_matrix,
                                           encoding_matrix))
unique_messages = np.delete(np.array(messages), dup_indexes)
##################################
greets = ["What's up?",
 'It is a pleasure to meet you.',
 'How do you do?',
 'Top of the morning to you!',
 'Hi',
 'How are you doing?',
 'Hello',
 'Greetings!',
 'Hi, How is it going?',
 'Hi, nice to meet you.',
 'Nice to meet you.']
greet_matrix = embed_fn(greets)
test_text = "Hey, how are you?"
test_embed = embed_fn([test_text])
np.inner(test_embed, greet_matrix)
sim_matrix  = np.inner(test_embed, greet_matrix)
if sim_matrix.max() > 0.8:
    print("it is a greetings")
else:
    print("it is not a greetings")
##########################################

