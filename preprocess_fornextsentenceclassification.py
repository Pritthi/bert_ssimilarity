import pandas as pd
import re
import string
import nltk
import numpy
nltk.download('averaged_perceptron_tagger')
ind_sentences=[]
data = pd.read_csv("cust_focus.csv")
print(data.head(2))
def flatten_to_strings(listOfLists):
    result = []
    for i in listOfLists:

        if isinstance(i, (str, bytes)):
            result.append(i)

        else:
            result.extend(flatten_to_strings(i))
    return result
def clean_text(text):
    ## Remove puncuation
    text = text.translate(string.punctuation)
    text=text.translate({ord(k): None for k in string.digits})
    ## Convert words to lower case and split them
    text = text.lower().split()
    # ## Remove stop words
    # stops = set(stopwords.words("english"))
    # text = [w for w in text if not w in stops and len(w) >= 3]
    text = " ".join(text)
    ## Clean the text
    text=re.sub(r"\+","",text)
    text = re.sub(r"[^A-Za-z0-9^,!.\/'+-=]", " ", text)
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " ", text)
    text = re.sub(r"e.g", "example", text)
    text=re.sub(r"-","",text)
    text = re.sub(r":", "", text)
    text = re.sub(r"!", "", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)
    text = re.sub(r",", " ", text)
    # text = re.sub(r"\.", " ", text)
    text = re.sub(r"!", " ! ", text)
    text = re.sub(r"\/", " ", text)
    text = re.sub(r"\^", " ^ ", text)
    text = re.sub(r"\+", " + ", text)
    text = re.sub(r"\-", " - ", text)
    text = re.sub(r"\=", " = ", text)
    text = re.sub(r"'", " ", text)
    text = re.sub(r"(\d+)(k)", r"\g<1>000", text)
    text = re.sub(r":", " : ", text)
    text = re.sub(r" e g ", " eg ", text)
    text = re.sub(r" b g ", " bg ", text)
    text = re.sub(r" u s ", " american ", text)
    text = re.sub(r"\0s", "0", text)
    text = re.sub(r" 9 11 ", "911", text)
    text = re.sub(r"e - mail", "email", text)
    text = re.sub(r"j k", "jk", text)
    text = re.sub(r"\s{2,}", " ", text)
    return text

# apply the above function to df['text']
data['Answer'] = data['Answer'].astype('str').map(lambda x: clean_text(x))
sentence_1=[]
sentence_2=[]
sentence_overall=[]

for tt in data['Answer']:
  # print("tt",tt)
  kk2=tt.split('.')
  kk=[i for i in kk2 if (i!='') and (i.isspace()==False) and (len(i)>1) and (i.rstrip().lstrip().isdigit()==False)]
  for yy in range(len(kk)):
      sentence_overall.append(kk[yy]) # overall all sentences
  if len(kk)>= 2: # if there are at least 2 sentences in the survey
      for gg in range(len(kk)-1):
             sentence_1.append(kk[gg])
             sentence_2.append(kk[gg+1])

sentence_1_false=[]
sentence_2_false=[]
print(sentence_1)
for tt in range(len(sentence_1)): ## to make balanced dataset, generate equal no of false pairs
    kk=numpy.random.randint(0, high=len(sentence_overall)-1)
    if kk+10<len(sentence_overall)-1:
        gg=numpy.random.randint(kk+10, high=len(sentence_overall)-1)## dont want pairs from the same comment

        sentence_1_false.append(sentence_overall[kk])
        print(len(sentence_1_false))
        sentence_2_false.append(sentence_overall[gg])
        print(len(sentence_2_false))

# print("lens1false",len(flatten_to_strings(sentence_1_false)))
# print("lens2false",len(flatten_to_strings(sentence_2_false)))
sentence_pair_1=[]
sentence_pair_1.extend(sentence_1)
sentence_pair_1.extend((sentence_1_false))
sentence_pair_2=[]
sentence_pair_2.extend(sentence_2)
sentence_pair_2.extend((sentence_2_false))
labels=numpy.ones(len(sentence_1)).tolist()
labels.extend(numpy.zeros(len((sentence_1_false))).tolist())
# print("len_s1",len(sentence_1))
# print("len s1false", len(flatten_to_strings(sentence_1_false)))
print("len sp1", len(sentence_pair_1))
print("len sp2",len(sentence_pair_2))
print("labels",len(labels))
new_df=pd.DataFrame({'sentence1':sentence_pair_1,'sentence2':sentence_pair_2,'label':labels})
new_df.to_csv('dataset_for_sentencepair.csv',index=False)
original_df=pd.DataFrame({'ss':sentence_overall}).to_csv('original_sentences.csv')