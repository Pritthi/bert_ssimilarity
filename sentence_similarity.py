import pandas as pd
# import nmslib
import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
import nltk
from nltk import tokenize
## load embeddings file
## find closest neighbor using a chosen metric
emb_bertmean_set=pd.read_csv('emb_glovemean_sentclsf_train.csv',header=0)
emb_bertmean=emb_bertmean_set.iloc[:,1:]
print(emb_bertmean.shape)
print(emb_bertmean.head(1))
sentence_set=pd.read_csv('train.csv',sep='\t',header=None)
sentences=sentence_set.iloc[:,1]
print(sentences.shape)
print(sentences.head(1))
# NTHREADS = 8
def get_closestneighbors(embs,no_neighbors):
    def create_index(a):
        index = nmslib.init(space='angulardist')
        index.addDataPointBatch(a)
        index.createIndex()
        return index
    def get_knns(index, vecs, k):
        return zip(*index.knnQueryBatch(vecs, k=k))
    nn_wvs = create_index(embs)
    to_frame = lambda x: pd.DataFrame(np.array(x)[:, 1:])
    idxs, dists = map(to_frame, get_knns(nn_wvs, embs, k=no_neighbors))
    catted = pd.concat([idxs.stack().to_frame('idx'), dists.stack().to_frame('dist')], axis=1).reset_index().drop('level_1', 1).rename(columns={'level_0': 'v1', 'idx': 'v2'})
    return catted

def innermat_remove_very_similar_sentences(encoding_matrix, messages):
    #################################
    # It takes similarity matrix (generated from sentence encoder) as input and gives index of redundant statements
    def redundant_sent_idx(sim_matrix):
        dup_idx = []
        for i in range(sim_matrix.shape[0]):
            if i not in dup_idx:
                tmp = [t + i + 1 for t in list(np.where(sim_matrix[i][i + 1:] > 0.8)[0])] # if similarity>0.8
                dup_idx.extend(tmp)
        return dup_idx

    # indexes of duplicate statements.
    dist_mat=cosine_similarity(encoding_matrix,encoding_matrix)
    pd.DataFrame(dist_mat).to_csv('dist.csv')
    dup_indexes = redundant_sent_idx(dist_mat)
    print(dup_indexes)
    unique_messages = np.delete(np.array(messages), dup_indexes)
    return unique_messages

def find_closest(encoding_matrix, messages,no_closest,index):
    dist_mat=cosine_similarity(encoding_matrix,encoding_matrix)
    pd.DataFrame(dist_mat).to_csv('dist.csv')
    closest_sentence=[]
    # for i in range(dist_mat.shape[0]):
        # print(i)
    i=int(index)
    sort_indices = np.argsort(dist_mat[i,:])[::-1]## most similar sentences
    # print(sort_indices[1])
    # print(messages.shape)
    kk=sort_indices[1:no_closest-1]
    print(kk)
    print(messages.shape[0])
    for gg in kk:
        print(gg)
        print(messages.iloc[gg]) # after itself
    return None

def kmeans_cluster(veccs,number_clusters):
    import matplotlib.pyplot as plt
    from sklearn.cluster import KMeans
    from sklearn.preprocessing import StandardScaler
    # scaler = StandardScaler()
    # data_scaled = scaler.fit_transform(veccs)
    data_scaled=veccs
    wcss = []
    for i in range(1, number_clusters):
        kmeans = KMeans(n_clusters=i, init='k-means++', random_state=0)
        kmeans.fit(data_scaled)
        wcss.append(kmeans.inertia_)
    plt.plot(range(1, number_clusters),wcss)
    plt.show()
    return None

def hierarchical_cluster(veccs):
    from sklearn.cluster import AgglomerativeClustering
    from scipy.cluster.hierarchy import dendrogram
    def plot_dendrogram(model, **kwargs):
        # Create linkage matrix and then plot the dendrogram
        # create the counts of samples under each node
        counts = np.zeros(model.children_.shape[0])
        n_samples = len(model.labels_)
        for i, merge in enumerate(model.children_):
            current_count = 0
            for child_idx in merge:
                if child_idx < n_samples:
                    current_count += 1  # leaf node
                else:
                    current_count += counts[child_idx - n_samples]
            counts[i] = current_count

        linkage_matrix = np.column_stack([model.children_, model.distances_,
                                          counts]).astype(float)

        # Plot the corresponding dendrogram
        dendrogram(linkage_matrix, **kwargs)
    model = AgglomerativeClustering(n_clusters=None,distance_threshold=0)
    model=model.fit(veccs)
    plot_dendrogram(model, truncate_mode='level', p=3)
    return None

# aa=innermat_remove_very_similar_sentences(emb_bertmean,sentences)
# pd.DataFrame(aa).to_csv('unique_comments.csv', index=False)
kmeans_cluster(emb_bertmean,200)
# from sklearn.manifold import TSNE
# X = np.array(emb_bertmean)
# X_embedded = TSNE(n_components=2).fit_transform(X)
# import matplotlib.pyplot as plt
# plt.plot(X_embedded[:, 0], X_embedded[:, 1],'rx')
# plt.show()
##TODO : scale the values in case mag matters
#from sklearn.preprocessing import StandardScaler
# scaler = StandardScaler()
# data_scaled = scaler.fit_transform(data)
# label1=kmeans.labels_
# pd.DataFrame(label1).value_counts() -- get no of comments in each cluster
# pd.DataFrame({'comments' : sentences, 'labels':label1}).to_csv('commentsandlabels.csv',index=False)
## use silhouette analysis
# cluster_labels = clusterer.fit_predict(X)
#
#     # The silhouette_score gives the average value for all the samples.
#     # This gives a perspective into the density and separation of the formed
#     # clusters
#     silhouette_avg = silhouette_score(X, cluster_labels)
# import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
# from sklearn.preprocessing import StandardScaler
# scaler = StandardScaler()
# data_scaled = scaler.fit_transform(emb_bertmean)
# kmeans = KMeans(n_clusters=10, init='k-means++', random_state=0)
# kmeans.fit_predict(data_scaled)
# label1 = kmeans.labels_
# aa=pd.DataFrame(label1)
# print(aa[0].value_counts())
# pd.DataFrame({'comments' : sentences, 'labels':label1}).to_csv('commentsandlabels.csv',index=False)
# from scipy.cluster import  hierarchy
# # Clustering
# X = emb_bertmean
# threshold = 0.2
# Z = hierarchy.linkage(X,"average", metric="cosine")
# C = hierarchy.fcluster(Z, threshold, criterion="distance")
# aa=pd.DataFrame({'comments':sentences,'cluster':C})
# aa.to_csv('clustering_cosine_0.2_glove.csv',index=False)
# print(aa['cluster'].value_counts())
# print(np.max(C))
hierarchy.dendrogram(Z)
import matplotlib.pyplot as plt
plt.show()
# from scipy.cluster.hierarchy import dendrogram, linkage
# Z = linkage(data_scaled)
# dendrogram(Z)
# hierarchical_cluster(emb_bertmean)
# index=input('enter index')
# print(sentences.iloc[int(index)])
# find_closest(emb_bertmean,sentences,5,index)
# new_df=pd.DataFrame({'original_sentences':sentences,'closest_sentences':bb})
# new_df.to_csv('sentencepairs_glove.csv')
# print(len(aa))
# print(aa)