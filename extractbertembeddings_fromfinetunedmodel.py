import subprocess
import pandas as pd
import csv
import json
import itertools
comments=pd.read_csv('cust_focus_comments.csv',header=True) ## read in the comments file
def functions_extract_embeddings():
    ## overalltoken and overall embedding list
    overall_tokens=[]
    overall_embeddings=[]
    ## call extract_features.py for each comment -- get the output json -- extract the tokens and embeddings
    for i in range(comments.shape[0]): ## for each comment
        print("comment number", i)
        ## create the input.txt file containing the comment
        pd.DataFrame(comments.iloc[i,0]).to_csv("aa.csv",index=False) ## that comment
        with open("input.txt", "w", encoding="utf-8") as my_output_file:
            with open("aa.csv", "r", encoding="utf-8", errors='ignore') as my_input_file:
                    [my_output_file.write(" ".join(row)+'\n') for row in csv.reader(my_input_file)]
            my_output_file.close()
        ## call the extract_features.py file to extract the embeddings
        subprocess.Popen(["python", "extract_features.py" "--input_file=input.txt", "--output_file=output.jsonl", "--vocab_file=vocab.txt", "--bert_config_file=bert_config.json", " --init_checkpoint=model.ckpt-500",
        "--layers=-2,-3", "--max_seq_length=128", "--batch_size=8"], shell=True)
        ## read in the output.jsonl file
        json_file_path ='output.jsonl'
        with open(json_file_path, 'r') as j:
            contents = json.loads(j.read())
        embs=[]
        all_tokens=[]
        features=contents['features']
        for tt in range(len(contents['features'])):
            if tt!=0: ## not taking the embedding of the CLS token
                token_emb_details=features[tt]
                if token_emb_details['token']!='SEP':
                    all_tokens.append(token_emb_details['token']) ## save all the tokens in the file for which we have embeddings
                    emb_values=token_emb_details['layers'] ## length of emb_values = no of layers whose activations we are taking
                    qq=[]
                    for gg in range(len(emb_values)):
                        xx=emb_values[gg]
                        qq.append(xx['values'])
                    rr=list(itertools.chain.from_iterable(qq))
                    embs.append(rr)
        ## delete the input.txt file
        subprocess.Popen(["rm","-rf","input.txt"], shell=True)
        ## delete the output.jsonl
        overall_tokens.append(all_tokens)
        subprocess.Popen(["rm","-rf","output.jsonl"], shell=True)
        overall_embeddings.append(embs)



# import itertools
# json_file_path ='output_cust_2.jsonl'
# with open(json_file_path, 'r') as j:
#     contents = json.loads(j.read())
#
# embs=[]
# all_tokens=[]
# features=contents['features']
# for tt in range(len(contents['features'])):
#     if tt!=0: ## not taking the embedding of the CLS token
#         token_emb_details=features[tt]
#         if token_emb_details['token']!='SEP':
#             all_tokens.append(token_emb_details['token'])
#             # emb_values=token_emb_details['layers'] ## length of emb_values = no of layers whose activations we are taking
#             # qq=[]
#             # for gg in range(len(emb_values)):
#             #     xx=emb_values[gg]
#             #     qq.append(xx['values'])
#             # rr=list(itertools.chain.from_iterable(qq))
#             # embs.append(rr)
# import pandas as pd
# pd.DataFrame(all_tokens).to_csv('all_tokens.csv',index=False)
## need to keep counter of which tokens are for each sentence
# import csv
# csv_file = input('Enter the name of your input file: ')
# txt_file = input('Enter the name of your output file: ')
# with open(txt_file, "w", encoding="utf-8") as my_output_file:
#     with open(csv_file, "r", encoding="utf-8", errors='ignore') as my_input_file:
#         [ my_output_file.write(" ".join(row)+'\n') for row in csv.reader(my_input_file)]
#     my_output_file.close()

## keep points whose distance < threshold
# lst = [complex(x,y) for x,y in lst] -- instead of distance we need to get the cosine similarity measure.
#
# new = []
# for n in lst:
#     if not new or abs(n - new[-1]) <= diff:  # same as in the first version
#         new.append(n)
# print(new)